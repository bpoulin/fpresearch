---
title: Barriers
---

There are a number of barriers to research. These are different for everyone. If we hope to break down these barriers, we need to recognize what they are.

# Lack of Funding

# Lack of Experience

# Lack of Confidence

# Funding

# Lack of Time

# Lack of Interest

# Lack of Expertise