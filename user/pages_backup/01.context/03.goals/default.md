---
title: Goals
---

**Patient-oriented** research benefits the patient.

**Locally Relevant** research will benefit patients and providers in our area.

**Valid** research is rigorous.