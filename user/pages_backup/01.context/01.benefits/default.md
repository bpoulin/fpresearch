---
title: Benefits
---

The potential benefits of research  are many. In particular, we propose research that has the following characteristics.

#Patient Oriented

Our research, by nature, will be patient-oriented.

#Relevant

Impact http://www.cfp.ca/content/62/3/266.full

#Valid

