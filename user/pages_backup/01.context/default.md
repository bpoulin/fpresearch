---
title: Context
---

# Why Research?

Research is important for **refining medical practice** for improvement of the lives of patients, medical practitioners, and their communities. It will allow us to be better and smarter than before. Improvements may include quality of care, efficiency, and happiness.