---
title: People
---

#  People

:fa-user-md: [Michael Klein](http://www.michaelcklein.ca) is a Family Physician, Pediatrician, Neonatologist, and Maternity Care Researcher.

:fa-user-md: [Patricia Janssen](http://bcchr.ca/our-research/researchers/results/Details/patricia-janssen) is a perinatal epidemiologist with a clinical background in obstetrical nursing. She undertakes clinical trials to evaluate methods of pregnancy and labour management and interventions for mothers at particularly high risk for experiencing adverse perinatal outcomes.

# Places



