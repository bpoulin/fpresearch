---
title: Home
visible: false
---

# Family Practice Research Home

## This is the research home for the SNO Division of Family Practice.

This is a living document covering information related to the development of a **primary care research program** in the Shuswap and North Okanagan.

It has been developed by **Brett Poulin**, the Director of Research for the SNO Division of Family Practice.

---

!!! Check **this document** for updates.
