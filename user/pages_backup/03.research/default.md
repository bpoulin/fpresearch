---
title: Research
---

# Current Research in SNO

There is no current research in SNO that I am aware of.

# Current Research in Vernon

Dr. Hamish Hwang has been pursuing research in General Surgery in the Vernon area. His research has included studies related to wait times for colonoscopy (T Chan, H Hwang, AA Karimuddin. Wait times for general surgery in BC: Moving beyond measurement. British Columbia Medical Journal. 2015; 57(8): 341-348.) and scheduling for general surgeons (H Hwang, A Barton. Computer randomized scheduling for general surgery: A novel tool for resource sharing at two regional hospitals in British Columbia. British Columbia Medical Journal. 2016; 58(1): 19-24.), among other topics. 

His research has influenced availability of care for our patients (increased funding for colonoscopies), as well as satisfaction for the general surgeons. It is this type of research that we hope to  pursue in family medicine.

# Current Research in Family Medicine

# Current Research in Primary Care

Research in Primary Care is less mature than in other areas of medicine. However, I feel that many future developments will be related to personal medicine, preventative medicine, efficiency, and costs. 