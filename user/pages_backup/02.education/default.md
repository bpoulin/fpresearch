---
title: Education
---

# Evidence and Education

In order to create good research, we need to understand what is current.

The list of resources below includes educational resources for BC family physicians that are considered to be best in their class. Family physicians can use these resources as a starting point for planning their Continuing Medical Education.

### Clinical Decision Support

{{ stars(1) }}
[Up To Date](http://uptodate.com) is a current, comprehensive, peer-reviewed, searchable resource providing specific, practical recommendations for diagnosis and treatment. It combines broad coverage of adult primary care, subspecialty internal medicine, ob/gyn, general surgery and pediatrics.    

### Email

{{ stars(2) }} 
[Daily POEMs](https://www.cma.ca/secure/en/Pages/poem.aspx) ("Patient-Oriented Evidence that Matters") are synopses of new evidence carefully filtered for relevance to patient care and evaluated for validity. Daily POEMs emerge from continuous review, grading, and critical appraisal of all 3000+ studies published monthly in more than 100 journals. It is sent to CMA members and published by Essential Evidence Plus.

### Conferences

{{ stars(3) }} 
[St. Paul's Hospital Continuing Medical Education Conference for Primary Care Physicians](http://stpaulscme.org) is an annual conference (November in Vancouver) in primary care medicine provides an opportunity to refresh your knowledge about, and enthusiasm for, primary care medicine. This conference will focus on an evidence based review of practical information, new trends and discuss controversial topics useful for primary care physicians practicing in urban or rural areas. 

### CME

{{ stars(4) }} 
["The McMaster Modules" by the Foundation for Medical Practice Education](https://www.fmpe.org/en) use a case-based, self-directed learning approach for practicing family physicians.

### Journals

{{ stars(5) }} 
[Canadian Family Physician](http://www.cfp.ca) is the official journal of The College of Family Physicians of Canada.


{{ stars(6) }} 
[The Annals of Family Medicine](http://www.annfammed.org) is a peer-reviewed research journal to meet the needs of scientists, practitioners, policymakers, and the patients and communities they serve.


{{ stars(7) }} 
[American Family Physician](http://www.aafp.org/journals/afp.html) is the  AAFP’s peer-reviewed, evidence-based clinical journal and offers concise, easy-to-read clinical review articles for physicians and other health care professionals.




