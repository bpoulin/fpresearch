---
title: UpToDate
---

[Up To Date](http://uptodate.com) is a current, comprehensive, peer-reviewed, searchable resource providing specific, practical recommendations for diagnosis and treatment. It combines broad coverage of adult primary care, subspecialty internal medicine, ob/gyn, general surgery and pediatrics. 