---
title: Home
visible: false
---

# Family Practice Research

![](KalDockIMG_3292.jpg)

!!! This is the **research home** for the Shuswap North Okanagan (SNO) Division of Family Practice.

### How can you get involved in family practice research?

##### Start [here](/research/get-started)!

### What is [FP Research](http://fpresearch.ca)?

[FP Research](http://fpresearch.ca) a living document with resources for development of **primary care research program** where you are. For us, this is the Shuswap and North Okanagan in British Columbia, Canada.

It contains resources to help a primary care physician perform locally relevant  research where you are. Please examine the resources in these pages to understand ...
	+ What is the [context](../context) of our family practice research? What has led us to provide this resource? What do we hope to provide? What do we hope to achieve?
	+ How can we do good research unless we understand the current state of the art? To do this, we need continuing medical [education](/education).
	+ What research is being done now? As we develop our research program, we want to share [current research](/research).
	+ How do you get started? We will point you in the [right direction](/resources/get-started). You will need to know what [resources](/resources) are available.
	
---
This resource has been developed by **Brett Poulin**, the Director of Research for the SNO Division of Family Practice.


