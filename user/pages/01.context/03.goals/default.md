---
title: Goals
---

The potential benefits of research  are many. In particular, we propose that research supported by the Divisions of Family practice should have the following characteristics.

#### ** Patient-Oriented **

Our research, by nature, will be patient-oriented.

#### ** Locally Relevant **

Our research should benefit patients and providers in our area. It will have local [impact](http://www.cfp.ca/content/62/3/266.full).

#### ** Valid **

Our research, regardless of scope, should be rigorous and valid.



