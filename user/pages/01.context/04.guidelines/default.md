---
title: Guidelines
---

We have guidelines for funding research projects. These are general guidelines, but each project will be considered on it's own merits. These will be further established by the SNO Division Board.