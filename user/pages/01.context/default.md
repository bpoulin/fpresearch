---
title: Context
---

# Why Research?

Research is important for **refining medical practice** and for improving the lives of patients, medical practitioners, and their communities. Research will stimulate us to work better and smarter than before. It has potential to improve quality of care, efficiency, and happiness for patients and practitioners.

> Family medicine research is the basis on which family medicine is built. It produces the evidence that supports our clinical practice and helps promote the value of family medicine as a cornerstone of the Canadian health care system. We produce, synthesize, and transfer the results of our research—the evidence—into best practice approaches to patient care. -- [The College of Family Physicians of Canada](http://www.cfpc.ca/research) 
