---
title: Barriers
---

There are a number of barriers to research. These are different for everyone. There are special barriers that apply to family physicians in practice, especially in  rural settings and small urban centers.


* **Lack of Time.** This will always be an obstacle. Time that you spend on research is time that you are not spending on your practice or at home. However, we hope that some funding options mean that you are not penalized financially for an interest in research, so that you can afford to make some time and follow your interests. In addition, we hope that providing this resource will help you make more efficient use of any time you might dedicate to research.

* **Lack of Funding.** There are a few  [funding sources](/resources/funding-sources) that you may not be aware of.

* **Lack of Experience.** You can [get started](/research/get-started), even if you don't have much experience. We'll help guide you through it.

* **Lack of Confidence.** Once again, we will help you through this.

* **Lack of Interest.** We hope that by making research accesible, you will start to consider areas of interest that you might not have thought about before.

* **Lack of Expertise.** We are happy to provide resources, mentoring and expertise. This is why we are developing this research program. 

* **Lack of Support.** Through our research program, we hope to make available the resources and support you need to get this done.