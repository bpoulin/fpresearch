---
title: 'Get Started'
---

For those who would like to consider research, we'd like to help you get started.

If you're not feeling ready to start somethings, but you'd still like to be involved in research, you might get started by collaborating with a [research network](/networks).

Start a project related to a research question. The steps include:

1. Have a research idea. Flesh it out.
2. Ask for help. Review the information in this site before you get started. If you are a SNO member, contact us! If you are not a SNO member, contact your local research support.
3. [Review the literature](/resources/search-the-literature).
4. Select a [design](/resources/study-design).
5. [Propose your research](/research/propose-a-project) Propose it for funding. We have a template. In the SNO Division we have [guidelines](../guidelines) for funding. In other organizations, there will also be guidelines and criteria.
6. Present it for consideration for [funding](/resources/funding-sources).
7. You may need to pursue [ethics approval](/resources/get-ethics/approval).
8. Do the research. Look for a mentor. Use [resources](/resources].
9. Write it up.
10. Share it.
11. Improve your practice and others.

We will help provide further support for SNO members along the way. Those who are not SNO members are welcome to use this site.
