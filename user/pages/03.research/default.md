---
title: Research
---

Are you interested in family practice research in the North Okanagan?

If you would like to participate in research, [start here](get-started).

If you are interested in research efforts in the SNO Division of Family Practice, check here for
[proposed](proposed), [ongoing](in-progress), and [published](published) research.

For some projects that are still in early consideration, please log in to the [Division Members Only Area](members-only).
