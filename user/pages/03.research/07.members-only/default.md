---
title: 'Division Members Only'
taxonomy:
    tag:
        - hidden
---

This section is intended for division members only. It contains information about proposed research projects for divisions members  and members of the board. These are currently under review and should not be shared with others without permission of the authors or the board.

# Proposals

How is ER use in Vernon affected by access to a family physician?
[Google Doc](https://docs.google.com/document/d/1_JPxo3tmQ1av59L_Tm9Kqk57MEp_JkknYqqei2IQjQM/pub)

Can we improve patient outcomes and decrease costs of treatment with low dose CT screening for those at risk for lung cancer?
[Google Doc](https://docs.google.com/document/d/1dEa8CLwehFhhekLthcGSJ08ljDgbiBtskgTy0TCSXS8/pub)

Are cancers being diagnosed more frequently in the ER than we might expect? Is this related to access to a GP?
[Google Doc](https://docs.google.com/document/d/18r4eLAh_mWKnu_EUt0XM_QXgqmBuHPdU1oQ_HbC85YI/pub)

How many children in the SNO area are without a GP, and what is their demographic profile? How does this affect their use of the health care system later on?
[Google Doc](https://docs.google.com/document/d/1opF9SNC83E3LpMevJtmbMglH3tguu5uauoIjdd7TX64/pub)

Are bounce-back rates after hospitalization higher for patients without a GP? Could a dedicated transition clinic reduce return to hospital within 30 days?
[Google Doc](https://docs.google.com/document/d/1fdDvEj3EC8jS7Z4f_Iz2BixNFhnYXkKUePe_XO6y-Zs/pub)