---
title: Proposed
---

# Emergency Room Use in Vernon

Emergency room resources are scarce. The emergency room volume in Vernon has  continued to increase, demanding more resources. These resource include space and staff, including physicians and nurses.  

Could the resources that are currently being used by the emergency room be more effectively used in a  different way? Specifically, is there a more efficient way to manage [CTAS](http://caep.ca/resources/ctas/implementation-guidelines) 4 (Semi-urgent) and 5 (Non-urgent) patients?

CTAS 4 : Less Urgent (Semi urgent) : Conditions that related to patient age, distress, or potential for deterioration or complications would benefit from intervention or reassurance within 1-2 hours).

CTAS 5 : Non Urgent : Conditions that may be acute but non-urgent as well as conditions which may be part of a chronic problem with or without evidence of deterioration. The investigation or interventions for some of these illnesses or injuries could be delayed or even referred to other areas of the hospital or health care system.


# Emergency Room Use by Practice Profile
