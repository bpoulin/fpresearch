---
title: 'Propose a Project'
---

## Propose a project

Select a topic, identify the research problem, and state a clear research question. Then write the proposal.Writing the proposal will help you work through the topics you need to consider.

The SNO Division of Family Practice will hear proposals and consider funding on a case by case basis. Funding will be considered  by the board at monthly board meetings. Please contact Brett Poulin or Tracey Kirkman if you are interested in a research proposal. 

For research proposals for the SNO board or for other organizations, the  structure will be similar. For the SNO board, we propose a short proposal of one to three pages.

You can find a Google Docs version at this [link](https://docs.google.com/document/d/16QBfnhFtT-TD00B1UxZcjyQAnZwkEv53ZodRdRAAm-g/pub), or you can use the template below.

#### Research Proposal Template

* **Proposed by**: _authors_
* **Proposal to**: _SNO Division of Family Practice_
* **Date**: _current date_

* **Proposed Research Question / Title**
	- A concise statement of the research question.
* **Abstract**
	- A brief, one-paragraph summary of the information below.
* **Goals of the Project **
	- Summarize the goals of your project. State the target population and expected impact/relevance.
* **Background** 
	- Review the background regarding the research question and goals of the project. This section may include a [summary of the peer-reviewed literature](/resources/search-the-literature) or other relevant material pointing to prior work and current gaps in knowledge. Why are you proposing this research?  Why is it relevant?
* **Methods**
	- Discuss the methods to be used, including [study design](/resources/study-design), materials, sample size and characteristics, as well as data collection and analysis.
* **Ethics**
	- Discuss whether [ethics approval](/resources/get-ethics-approval) is needed, and any special considerations.
* **Proposed Timeline**
	- Discuss the timeline of the project and expected major milestones.
* **Proposed Budget and Resources**
	- Discuss the budget required with a summary of the anticipated costs. If you need the additional resources of the SNO Division of Family Practice, please note the resources required. If any further human resources, including clinical staff or student time might be required, include that here.
* **References**
	- Refer to relevant resources in a reasonable format.

## Some other resources

[Tips for a Research Project](https://medicine.dal.ca/content/dam/dalhousie/pdf/faculty/medicine/departments/department-sites/family/Education%20Documents/resident_resources/projects/DFM_Resident_Project_Tips.pdf)

An Evidence-Based Guide to Writing Grant Proposals for Clinical Research
Sharon K. Inouye, MD, MPH; David A. Fiellin, MD
Article, Author, and Disclosure Information

