---
title: 'Design the Study'
---

You will want to select from a variety of study designs. Each has their merits and challenges. Some resources below may be helpful.

[Research Methods Knowledge Base](http://www.socialresearchmethods.net/kb/), a comprehensive web-based textbook by William M. K. Trochim.

[Introduction to epidemiology and study designs](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/Kaczorowski_epidemiologyPPT21630.pdf), presentation by Janusz Kaczorowski.

Online Research Methods Course formerly offered through the Departments of Family Practice and Paediatrics at UBC. We appreciate that they have made these available online. There are a number of modules available online, including:

[Refining a Research Question](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/RefiningResearchQ21710.pdf)

[Searching the Literature](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/LiteratureSearching21711.pdf)

[Study Designs I](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/StudyDesignsI21713.pdf)

[Study Designs II (Surveys)](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/StudyDesignsII217141.pdf)

[Study Designs III (Qualitative Designs)](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/StudyDesignsIII21715.pdf)

[Sample Size Considerations](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/SampleSizeConsiderations217161.pdf)

[Implementation Issues](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/ImplementationIssues21718.pdf)

[Data Management](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/RMC_data_management7806.pdf)

[Refining Grant Writing](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/RMC_final_grant_writing7808-1.pdf)
