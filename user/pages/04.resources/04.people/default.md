---
title: 'Find Research People'
---

#  People

:fa-user-md: [Michael Klein](http://www.michaelcklein.ca) is a Family Physician, Pediatrician, Neonatologist, and Maternity Care Researcher.

:fa-user-md: [Patricia Janssen](http://bcchr.ca/our-research/researchers/results/Details/patricia-janssen) is a perinatal epidemiologist with a clinical background in obstetrical nursing. She undertakes clinical trials to evaluate methods of pregnancy and labour management and interventions for mothers at particularly high risk for experiencing adverse perinatal outcomes.

:fa-usermd: [Morgan Price](https://www.uvic.ca/medsci/people/faculty/pricemorgan.php) Dr. Morgan Price, MD, PhD, CCFP, FCFP is an assistant professor (University of British Columbia, Department of Family Practice, Island Medical Program) and family physician practicing in the inner city with under-served populations in Victoria. Dr. Price is an Adjunct Professor in Computer Science and Health Information Science at the University of Victoria. His area of interest is in design and adoption research of clinical information systems, focused in primary care, decision support, and consumer oriented information systems (Personal Health Services). He has been Primary Investigator on several successful clinical informatics design and research projects exploring clinical decision support, EMR adoption, personal health records, and medication information systems.

:fa-usermd: Dr. Douglas Kingsford as the new Executive Medical Director and Chief Medical Information Officer of the Interior Health Authority. Dr Kingsford is a primary care physician with a New Zealand Fellowship in General Practice, an Electronic Engineering PhD in artificial intelligence, and extensive experience in clinical medicine and software engineering, primarily in clinical informatics.

## Key People in the Shuswap North Okanagan Area

Aubrie De Beer, Family Physician, Chief of Staff, Vernon Jubilee Hospital
Hamish Hwang, General Surgeon with Research Interests
Chris Cunningham, Family Physician, Active

# Organizations

[The Department of Family Practice Research Office at UBC](http://research.familymed.ubc.ca)

The [Health Data Coalition](http://hdcbc.ca) (HDC) – an amalgamation of the knowledge, assets, and experiences gained within the Physicians Data Collaborative (PDC) and AMCARE – is a physician-led data sharing initiative funded by the General Practice Services Committee (GPSC).

[SCOOP](http://scoop.leadlab.ca) is the start of a primary care research network at University of British Columbia’s Department of Family Practice. We are connecting EMR using primary care practices to answer clinical questions. The initial focus will be on medication safety and EMR data quality. We are working closely with the Physicians Data Collaborative Society of BC.

[The College of Family Physicians of Canada](http://www.cfpc.ca/research/) has a research department.

# Places

[Shuswap Lake General Hospital](https://www.interiorhealth.ca/FindUs/_layouts/FindUs/info.aspx?type=Location&loc=Shuswap%20Lake%20General%20Hospital&svc=Pulmonary%20Diagnostics&ploc=N/A)
[Vernon Jubilee Hospital](https://www.interiorhealth.ca/FindUs/_layouts/FindUs/info.aspx?type=Location&loc=Vernon+Jubilee+Hospital)



