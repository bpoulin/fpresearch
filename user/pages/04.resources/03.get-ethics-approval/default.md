---
title: 'Get Ethics Approval'
---

The Tri-Council - Canadian Institutes for Health Research (CIHR), Social Science and Humanities (SSHRC) and National Science and Engineering Research Council (NSERC) – has developed a joint research ethics policy.

[Tri-Council Policy Statement 2 (2014): Ethical Conduct for Research Involving Humans](http://www.pre.ethics.gc.ca/eng/policy-politique/initiatives/tcps2-eptc2/Default/)

Chart reviews, or chart audits, also require REB approval when the resident is planning to discuss the results publicly (Resident Project Day). If a Chart audit is only used to improve the practice, no REB approval is required.

UBC-affiliated institutions, hospitals, and researchers will need to know [how to obtain UBC ethics approval](https://ethics.research.ubc.ca/about-human-research-ethics/how-obtain-ethics-approval).

[Guidance Note](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/Guidance-Note-UBC-FPR-and-SMS-FINAL-1.doc) on UBC Family Practice Resident Research and UBC Summer Medical Students Research Projects.

In the Interior Health Authority, you will need to submit to the [Research Ethics Board](https://www.interiorhealth.ca/AboutUs/ResearchandEthics/Pages/REB.aspx). Note that IHA has an agreement with UBC so you should only have to submit to one board and then notify the other institution that it has been approved.
