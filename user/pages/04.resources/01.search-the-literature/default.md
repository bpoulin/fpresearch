---
title: 'Search the Literature'
---

In addition to usual CME sources, you will want to search the primary literature as you investigate your question. There are a number of resources available for this purpose.

[Searching the Literature](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/RMC_literature_searching7804.pdf), a module for the Online Research Methods Course formerly offered through the Departments of Family Practice and Paediatrics at UBC.

[Systematic Reviews and Meta-analysis: An Introduction](http://med-fom-familymed-research.sites.olt.ubc.ca/files/2012/03/Systematic_Review100391.pdf), presentation by Janusz Kaczorowski.

[PubMed Tutorial](https://www.nlm.nih.gov/bsd/disted/pubmedtutorial/cover.html) (essential).

[RefWorks](http://resources.library.ubc.ca/901/), citation management software free for UBC students and faculty.

[Family Practice Research Guide](http://guides.library.ubc.ca/familypractice) at the UBC Library
